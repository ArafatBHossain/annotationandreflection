package net.therap.validator.main;

import net.therap.validator.domain.Person;
import net.therap.validator.domain.ValidationError;
import net.therap.validator.utility.AnnotatedValidator;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author arafat
 * @since 11/7/16
 */
public class Main {

    public static void main(String[] args) throws NoSuchMethodException, SecurityException, InvocationTargetException,
            IllegalAccessException {

        Person personObj = new Person("Abcsdfsdfsdfssadadsasdfde", 139);

        List<ValidationError> errors = AnnotatedValidator.validate(personObj);

        AnnotatedValidator.print(errors);

    }

  }
