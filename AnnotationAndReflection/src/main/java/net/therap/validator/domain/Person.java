package net.therap.validator.domain;

import net.therap.validator.annotation.Size;

/**
 * @author arafat
 * @since 11/7/16
 */
public class Person {

    @Size(max = 20)
    private String name;

    @Size(min = 16,max = 40)
    private int age;


    public Person(String name, int age) {

        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
