package net.therap.validator.domain;

/**
 * @author arafat
 * @since 11/7/16
 */
public class ValidationError {

    private String message;
    private String fieldName;
    private String fieldType;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }
}
