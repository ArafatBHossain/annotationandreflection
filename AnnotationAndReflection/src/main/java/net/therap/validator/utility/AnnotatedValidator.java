package net.therap.validator.utility;

import net.therap.validator.annotation.Size;
import net.therap.validator.domain.ValidationError;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author arafat
 * @since 11/7/16
 */
public class AnnotatedValidator {

    private static final String STRING = "String";
    private static final String INTEGER = "int";
    private static final String REGEXMAX = "\\{max\\}";
    private static final String REGEXMIN = "\\{min\\}";

    public static List<ValidationError> validate(Object targetObject) {

        Field[] fields = targetObject.getClass().getDeclaredFields();
        List<ValidationError> validationErrorList = new ArrayList<>();

        try {

            for (Field field : fields) {

                field.setAccessible(true);
                ValidationError validationErrorObject;

                if (field.isAnnotationPresent(Size.class)) {

                    validationErrorObject = processSizeAnnotation(field, targetObject);

                    if (validationErrorObject.getMessage() != null) {
                        validationErrorList.add(validationErrorObject);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return validationErrorList;
    }

    public static ValidationError processSizeAnnotation(Field field, Object targetObject) throws
            IllegalAccessException {

        Annotation[] annotations = field.getDeclaredAnnotations();

        ValidationError validationError = new ValidationError();

        Object valueOfField = field.get(targetObject);
        Object typeOfField = field.getType();

        int min, max;

        String processedMessage;
        String fieldName = field.getName();

        for (Annotation annotation : annotations) {

            Size sizeAnnotation = (Size) annotation;

            max = sizeAnnotation.max();
            min = sizeAnnotation.min();


            if (annotation instanceof Size) {

                if (typeOfField.equals(String.class)) {

                    String s = (String) valueOfField;

                    if (s.length() < min | s.length() > max) {
                        processedMessage = processMessage(sizeAnnotation.message(), max, min);
                        validationError.setFieldType(STRING);
                        validationError.setFieldName(fieldName);
                        validationError.setMessage(validationError.getFieldType() +
                                "(" + validationError.getFieldName() + "): " + processedMessage);
                    }

                } else if (typeOfField.equals(int.class)) {

                    int i = (Integer) valueOfField;

                    if (i < min || i > max) {
                        processedMessage = processMessage(sizeAnnotation.message(), max, min);
                        validationError.setFieldType(INTEGER);
                        validationError.setFieldName(fieldName);
                        validationError.setMessage(validationError.getFieldType() + "("
                                + validationError.getFieldName() + "): " + processedMessage);
                    }
                }
            }
        }

        return validationError;
    }

    public static void print(List<ValidationError> errors) {

        if (errors.size() != 0) {
            for (ValidationError validationError : errors) {
                System.out.println(validationError.getMessage());
            }
        }
    }

    public static String processMessage(String message, int max, int min) {

        return message.replaceAll(REGEXMIN, String.valueOf(min)).replaceAll(REGEXMAX, String.valueOf(max));

    }

}
